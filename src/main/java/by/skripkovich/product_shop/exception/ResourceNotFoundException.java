package by.skripkovich.product_shop.exception;

public class ResourceNotFoundException extends RuntimeException {

    public ResourceNotFoundException(Object o) {
        super("Resource not found: " + o);
    }
}
