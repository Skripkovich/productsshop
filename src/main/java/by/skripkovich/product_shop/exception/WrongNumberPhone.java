package by.skripkovich.product_shop.exception;

public class WrongNumberPhone extends RuntimeException {

    public WrongNumberPhone(Object o) {
        super("Wrong number phone: " + o);
    }
}
