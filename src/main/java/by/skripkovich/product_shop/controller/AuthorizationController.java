package by.skripkovich.product_shop.controller;

import by.skripkovich.product_shop.dao.dto.UserDtoRequest;
import by.skripkovich.product_shop.dao.dto.UserDtoResponse;
import by.skripkovich.product_shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//TODO Отлавить эксепшены
@RestController
@RequestMapping(value = "/authorization")
public class AuthorizationController {

    private final UserService userService;

    @Autowired
    public AuthorizationController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(produces = "application/json")
    public ResponseEntity<UserDtoResponse> authorization(@RequestBody UserDtoRequest request) {

        return ResponseEntity.ok(userService.authorization(request));
    }
}
