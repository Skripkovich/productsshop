package by.skripkovich.product_shop.controller;

import by.skripkovich.product_shop.dao.dto.ProductDtoResponse;
import by.skripkovich.product_shop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//TODO Отлавить эксепшены
@RestController
@RequestMapping(value = "/products")
public class ProductController {

    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(value = "/{subCategory}", produces = "application/json")
    public ResponseEntity<List<ProductDtoResponse>> getProducts(@PathVariable String subCategory) {

        return ResponseEntity.ok(productService.findBySubCategory(subCategory));
    }
}
