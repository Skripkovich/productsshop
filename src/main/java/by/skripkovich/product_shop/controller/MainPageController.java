package by.skripkovich.product_shop.controller;

import by.skripkovich.product_shop.dao.dto.CategoryDtoResponse;
import by.skripkovich.product_shop.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

//TODO Отлавить эксепшены
@RestController
@RequestMapping(value = "/")
public class MainPageController {

    private CategoryService categoryService;

    @Autowired
    public MainPageController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<Set<CategoryDtoResponse>> allCategory() {

        return ResponseEntity.ok(categoryService.getCategory());
    }
}
