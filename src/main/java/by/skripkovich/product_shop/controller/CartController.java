package by.skripkovich.product_shop.controller;

import by.skripkovich.product_shop.dao.dto.OrderDtoRequest;
import by.skripkovich.product_shop.dao.dto.OrderDtoResponse;
import by.skripkovich.product_shop.dao.entity.Order;
import by.skripkovich.product_shop.exception.WrongNumberPhone;
import by.skripkovich.product_shop.service.OrderService;
import by.skripkovich.product_shop.service.UserService;
import by.skripkovich.product_shop.service.convert.CommonConvert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//TODO Отлавить эксепшены
@RestController
@RequestMapping(value = "/cart")
public class CartController {

    private final OrderService orderService;
    private final CommonConvert<Order, OrderDtoResponse, OrderDtoRequest> convert;
    private final UserService userService;

    @Autowired
    public CartController(OrderService orderService, UserService userService, CommonConvert<Order, OrderDtoResponse, OrderDtoRequest> convert) {
        this.orderService = orderService;
        this.userService = userService;
        this.convert = convert;
    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<?> getAll() {
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(produces = "application/json")
    public ResponseEntity<?> createOrder(@RequestBody OrderDtoRequest request) {
        try {
            userService.addOrder(request);
        } catch (WrongNumberPhone e) {
            orderService.save(request);
        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
