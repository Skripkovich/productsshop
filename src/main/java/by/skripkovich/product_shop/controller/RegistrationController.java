package by.skripkovich.product_shop.controller;

import by.skripkovich.product_shop.dao.dto.UserDtoRequest;
import by.skripkovich.product_shop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

//TODO Отлавить эксепшены
@RestController
@RequestMapping(value = "/registration")
public class RegistrationController {

    private final UserService userService;

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<?> get () {

        return new ResponseEntity<>(HttpStatus.OK);
    }


    @PostMapping(produces = "application/json")
    public ResponseEntity<?> createUser(@Valid @RequestBody UserDtoRequest request) {

        userService.createNewUser(request);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
