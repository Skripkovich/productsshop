package by.skripkovich.product_shop.type;

public enum OrderStatus {

    PROCESSED("В процессе"),
    ON_THE_WAY("В пути"),
    DELIVERED ("Доставлен");

    private String orderStatus;

    OrderStatus (String orderStatusText){
        orderStatus = orderStatusText;
    }


    @Override
    public String toString() {
        return orderStatus;
    }
}
