package by.skripkovich.product_shop.type;

public enum LevelAccess {

    ADMIN("Admin"),
    USER("User"),
    BANNED("Banned");

    private String levelAccess;


    LevelAccess(String levelAccessText) {
        this.levelAccess = levelAccessText;
    }

    @Override
    public String toString() {
        return  levelAccess;
    }
}
