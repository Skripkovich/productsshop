package by.skripkovich.product_shop.type;

public enum UnitType {

    TONS("т."),
    KILOGRAM("кг."),
    PIECES("шт."),
    LITTER("литр.");

    private final String unitType;


    UnitType(String unitTypeText) {
        this.unitType = unitTypeText;
    }

    @Override
    public String toString() {
        return  unitType;
    }

}
