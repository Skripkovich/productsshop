package by.skripkovich.product_shop.dao.dto;

import by.skripkovich.product_shop.dao.entity.SubCategory;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class CategoryDtoRequest {
    private Long id;

    @NotNull(message = "Name of category has not must been is null")
    @NotEmpty(message = "Name of category has not must been is empty")
    private String categoryName;

    private List<SubCategory> subCategories = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<SubCategory> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<SubCategory> subCategories) {
        this.subCategories = subCategories;
    }
}
