package by.skripkovich.product_shop.dao.dto;

public class SubCategoryDtoResponse {

    private Long id;

    private String subCategoryName;

    public SubCategoryDtoResponse() {
    }

    public SubCategoryDtoResponse(Long id, String subCategoryName) {
        this.id = id;
        this.subCategoryName = subCategoryName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }
}
