package by.skripkovich.product_shop.dao.dto;

import by.skripkovich.product_shop.dao.embeded.Address;
import by.skripkovich.product_shop.dao.entity.Order;
import by.skripkovich.product_shop.type.LevelAccess;

import java.util.ArrayList;
import java.util.List;

public class UserDtoResponse {

    private Long id;
    private String email;
    private String name;
    private LevelAccess levelAccess;
    private Address address;
    private String numberPhone;
    private List<Order> orders = new ArrayList<>();

    public UserDtoResponse() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LevelAccess getLevelAccess() {
        return levelAccess;
    }

    public void setLevelAccess(LevelAccess levelAccess) {
        this.levelAccess = levelAccess;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public String getNumberPhone() {
        return numberPhone;
    }

    public void setNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
    }
}
