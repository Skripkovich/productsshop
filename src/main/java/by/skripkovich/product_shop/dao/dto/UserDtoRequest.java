package by.skripkovich.product_shop.dao.dto;

import by.skripkovich.product_shop.dao.embeded.Address;
import by.skripkovich.product_shop.dao.entity.Order;
import by.skripkovich.product_shop.type.LevelAccess;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

public class UserDtoRequest {

    private Long id;

    @NotNull(message = "Email has not must been is null")
    @NotEmpty(message = "Email has not must been is empty")
    @Email(message = "Invalid email",
            regexp = "^([a-z0-9_-]+\\.)*[a-z0-9_-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*\\.[a-z]{2,6}$")
    private String email;

    @NotNull(message = "Password has not must been is null")
    @NotEmpty(message = "Password has not must  been is empty")
    @Size(min = 8, max = 255, message = "Password has must minimum 8 and maximum 255 symbols")
    private String password;

    @NotNull(message = "Password has not must  been is null")
    @NotEmpty(message = "Password has not must  been is empty")
    @Size(min = 3, max = 255, message = "Name has must minimum 3 and maximum 255 symbols")
    private String name;

    @NotNull
    @NotEmpty(message = "Phone number has not must been is empty")
    @Size(min = 13, max = 13)
    private String numberPhone;

    private LevelAccess levelAccess;
    private Address address;
    private List<Order> orders = new ArrayList<>();

    public UserDtoRequest() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LevelAccess getLevelAccess() {
        return levelAccess;
    }

    public void setLevelAccess(LevelAccess levelAccess) {
        this.levelAccess = levelAccess;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getNumberPhone() {
        return numberPhone;
    }

    public void setNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
    }
}
