package by.skripkovich.product_shop.dao.dto;

import by.skripkovich.product_shop.dao.entity.Product;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class SubCategoryDtoRequest {

    private Long id;

    @NotNull(message = "Name of subCategory has not must been is null")
    @NotEmpty(message = "Name of subCategory has not must been is empty")
    private String subCategoryName;

    private List<Product> products = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
