package by.skripkovich.product_shop.dao.dto;

import java.util.ArrayList;
import java.util.List;

public class CategoryDtoResponse {

    private Long id;
    private String categoryName;
    private List<SubCategoryDtoResponse> subCategories = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<SubCategoryDtoResponse> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<SubCategoryDtoResponse> subCategories) {
        this.subCategories = subCategories;
    }
}
