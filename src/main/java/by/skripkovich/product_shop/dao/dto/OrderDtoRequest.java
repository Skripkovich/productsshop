package by.skripkovich.product_shop.dao.dto;

import by.skripkovich.product_shop.dao.embeded.Address;
import by.skripkovich.product_shop.dao.entity.Product;
import by.skripkovich.product_shop.type.OrderStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderDtoRequest {

    private Long numberOrder;
    private Date date;
    private OrderStatus orderStatus;
    private String numberPhone;
    private Address address;
    private String name;
    private List<Product> products = new ArrayList<>();

    public OrderDtoRequest() {
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Long getNumberOrder() {
        return numberOrder;
    }

    public void setNumberOrder(Long numberOrder) {
        this.numberOrder = numberOrder;
    }

    public String getNumberPhone() {
        return numberPhone;
    }

    public void setNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
