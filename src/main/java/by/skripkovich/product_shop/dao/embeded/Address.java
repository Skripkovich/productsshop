package by.skripkovich.product_shop.dao.embeded;

import javax.persistence.Embeddable;

@Embeddable
public class Address {

    private String city;
    private String street;
    private Integer house;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getHouse() {
        return house;
    }

    public void setHouse(Integer house) {
        this.house = house;
    }
}
