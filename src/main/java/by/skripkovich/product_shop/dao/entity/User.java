package by.skripkovich.product_shop.dao.entity;

import by.skripkovich.product_shop.dao.embeded.Address;
import by.skripkovich.product_shop.type.LevelAccess;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "email")
    @NotNull
    @Email
    private String email;

    @Column(name = "password")
    @NotNull
    @Size(min = 8, max = 255, message = "Password must have minimum 8 and maximum 255 symbols")
    private String password;

    @Column(name = "name")
    @NotNull
    @Size(min = 3, max = 255, message = "Name must have minimum 3 and maximum 255 symbols")
    private String name;

    @Column(name = "number_phone")
    @NotNull
    @Size(min = 13, max = 13)
    private String numberPhone;

    @Column(name = "lvl_access")
    @NotNull
    @Enumerated(EnumType.STRING)
    private LevelAccess levelAccess;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "city", column = @Column(name = "city")),
            @AttributeOverride(name = "street", column = @Column(name = "street")),
            @AttributeOverride(name = "house", column = @Column(name = "house"))
    })
    private Address address;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    //TODO переписать на SET?? + аннотация LazyCollection
    private List<Order> orders = new ArrayList<>();

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LevelAccess getLevelAccess() {
        return levelAccess;
    }

    public void setLevelAccess(LevelAccess levelAccess) {
        this.levelAccess = levelAccess;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public String getNumberPhone() {
        return numberPhone;
    }

    public void setNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
    }
}
