package by.skripkovich.product_shop.dao.entity;

import by.skripkovich.product_shop.type.UnitType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Entity
@Table(name = "products")
//@TypeDef(name = "unitType", typeClass = PostgreSQLEnumType.class)
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "product_name")
    @NotNull
    @Size(min = 3, max = 255, message = "Product name must have minimum 3 and maximum 255 symbols")
    private String productName;

    @Column(name = "description")
    @NotNull
    @Size(min = 3, max = 255, message = "Description must have minimum 3 and maximum 255 symbols")
    private String description;

    @Column(name = "weight")
    @NotNull
    private Float weight;

    @Column(name = "price")
    @NotNull
    private BigDecimal price;

    @Column(name = "image")
    private String image;

    @Column(name = "unit_type")
    @NotNull
    @Enumerated(EnumType.STRING)
//    @Type(type = "unitType")
    private UnitType unitType;

    @Column(name = "sub_category_id")
    private Long subCategoryId;

    public Product() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getWeight() {
        return weight;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public UnitType getUnitType() {
        return unitType;
    }

    public void setUnitType(UnitType unitType) {
        this.unitType = unitType;
    }

    public Long getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(Long subCategoryId) {
        this.subCategoryId = subCategoryId;
    }
}
