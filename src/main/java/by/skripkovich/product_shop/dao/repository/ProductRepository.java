package by.skripkovich.product_shop.dao.repository;

import by.skripkovich.product_shop.dao.entity.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.Lob;
import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    List<Product> findBySubCategoryId(Long id);

}
