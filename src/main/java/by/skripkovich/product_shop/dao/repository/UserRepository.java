package by.skripkovich.product_shop.dao.repository;

import by.skripkovich.product_shop.dao.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByEmail(String email);

    Optional<User> findByNumberPhone(String numberPhone);
}
