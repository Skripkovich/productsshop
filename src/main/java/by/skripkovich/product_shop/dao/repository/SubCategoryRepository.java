package by.skripkovich.product_shop.dao.repository;

import by.skripkovich.product_shop.dao.entity.SubCategory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubCategoryRepository extends CrudRepository<SubCategory, Long> {
    List<SubCategory> findBySubCategoryName (String subCategoryName);
}
