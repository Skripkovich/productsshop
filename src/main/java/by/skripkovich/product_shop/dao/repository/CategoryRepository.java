package by.skripkovich.product_shop.dao.repository;

import by.skripkovich.product_shop.dao.entity.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {
}
