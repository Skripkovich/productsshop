package by.skripkovich.product_shop.dao.repository;

import by.skripkovich.product_shop.dao.entity.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends CrudRepository<Order,Long> {
}
