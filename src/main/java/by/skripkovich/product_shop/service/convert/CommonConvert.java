package by.skripkovich.product_shop.service.convert;

public interface CommonConvert<T1,T2,T3> {

    T2 toDtoResponse(T1 entity);
    T3 toDtoRequest(T1 entity);
    T1 responseToEntity(T2 resp);
    T1 requestToEntity(T3 req);
}
