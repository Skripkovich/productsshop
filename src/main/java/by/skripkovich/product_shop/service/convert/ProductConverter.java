package by.skripkovich.product_shop.service.convert;

import by.skripkovich.product_shop.dao.dto.ProductDtoRequest;
import by.skripkovich.product_shop.dao.dto.ProductDtoResponse;
import by.skripkovich.product_shop.dao.entity.Product;
import by.skripkovich.product_shop.type.UnitType;
import org.springframework.stereotype.Service;

@Service
public class ProductConverter implements CommonConvert<Product, ProductDtoResponse, ProductDtoRequest> {

    private final Product product = new Product();
    private final ProductDtoResponse response = new ProductDtoResponse();
    private final ProductDtoRequest request = new ProductDtoRequest();

    @Override
    public ProductDtoResponse toDtoResponse(Product entity) {


        response.setDescription(entity.getDescription());
        response.setImage(entity.getImage());
        response.setPrice(entity.getPrice());
        response.setProductName(entity.getProductName());
        response.setUnitType(entity.getUnitType().toString());
        response.setWeight(entity.getWeight());
        response.setSubCategoryId(entity.getSubCategoryId());

        return response;
    }

    @Override
    public ProductDtoRequest toDtoRequest(Product entity) {

        request.setDescription(entity.getDescription());
        request.setImage(entity.getImage());
        request.setPrice(entity.getPrice());
        request.setProductName(entity.getProductName());
        request.setId(entity.getId());
        request.setUnitType(entity.getUnitType().toString());
        request.setWeight(entity.getWeight());
        request.setSubCategoryId(entity.getSubCategoryId());

        return request;
    }

    @Override
    public Product responseToEntity(ProductDtoResponse resp) {

        product.setDescription(resp.getDescription());
        product.setImage(resp.getImage());
        product.setPrice(resp.getPrice());
        product.setProductName(resp.getProductName());
        product.setUnitType(UnitType.valueOf(resp.getUnitType()));
        product.setWeight(resp.getWeight());
        product.setSubCategoryId(resp.getSubCategoryId());

        return product;
    }

    @Override
    public Product requestToEntity(ProductDtoRequest req) {

        product.setDescription(req.getDescription());
        product.setImage(req.getImage());
        product.setPrice(req.getPrice());
        product.setProductName(req.getProductName());
        product.setUnitType(UnitType.valueOf(req.getUnitType()));
        product.setWeight(req.getWeight());
        product.setId(req.getId());
        product.setSubCategoryId(req.getSubCategoryId());

        return product;
    }
}
