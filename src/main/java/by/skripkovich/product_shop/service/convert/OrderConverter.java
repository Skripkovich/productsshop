package by.skripkovich.product_shop.service.convert;

import by.skripkovich.product_shop.dao.dto.OrderDtoRequest;
import by.skripkovich.product_shop.dao.dto.OrderDtoResponse;
import by.skripkovich.product_shop.dao.entity.Order;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class OrderConverter implements CommonConvert<Order, OrderDtoResponse, OrderDtoRequest> {

    @Override
    public OrderDtoResponse toDtoResponse(Order entity) {
        OrderDtoResponse response = new OrderDtoResponse();

        response.setNumberOrder(entity.getNumberOrder());
        response.setDate(entity.getDate());
        response.setOrderStatus(entity.getOrderStatus());
        response.setProducts(entity.getProducts());
        response.setAmount(entity.getProducts().size());
        response.setAddress(entity.getAddress());
        response.setName(entity.getName());
        response.setNumberPhone(entity.getNumberPhone());

        BigDecimal sum = new BigDecimal(0);
        for (int i = 0; i < entity.getProducts().size(); i++) {
            sum = sum.add(entity.getProducts().get(i).getPrice());
        }

        response.setSum(sum);
        return response;
    }

    @Override
    public OrderDtoRequest toDtoRequest(Order entity) {
        OrderDtoRequest request = new OrderDtoRequest();
        request.setDate(entity.getDate());
        request.setOrderStatus(entity.getOrderStatus());
        request.setProducts(entity.getProducts());
        request.setAddress(entity.getAddress());
        request.setName(entity.getName());
        request.setNumberPhone(entity.getNumberPhone());
        return request;
    }

    @Override
    public Order responseToEntity(OrderDtoResponse resp) {
        Order order = new Order();
        order.setNumberOrder(resp.getNumberOrder());
        order.setDate(resp.getDate());
        order.setOrderStatus(resp.getOrderStatus());
        order.setProducts(resp.getProducts());
        order.setAddress(resp.getAddress());
        order.setName(resp.getName());
        order.setNumberPhone(resp.getNumberPhone());

        return order;
    }

    @Override
    public Order requestToEntity(OrderDtoRequest req) {
        Order order = new Order();
        order.setDate(req.getDate());
        order.setOrderStatus(req.getOrderStatus());
        order.setProducts(req.getProducts());
        order.setAddress(req.getAddress());
        order.setNumberPhone(req.getNumberPhone());
        order.setName(req.getName());

        return order;
    }
}
