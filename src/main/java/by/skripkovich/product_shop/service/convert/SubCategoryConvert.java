package by.skripkovich.product_shop.service.convert;

import by.skripkovich.product_shop.dao.dto.SubCategoryDtoRequest;
import by.skripkovich.product_shop.dao.dto.SubCategoryDtoResponse;
import by.skripkovich.product_shop.dao.entity.SubCategory;
import org.springframework.stereotype.Service;

@Service
public class SubCategoryConvert implements CommonConvert<SubCategory, SubCategoryDtoResponse, SubCategoryDtoRequest> {


    @Override
    public SubCategoryDtoResponse toDtoResponse(SubCategory entity) {
        SubCategoryDtoResponse response = new SubCategoryDtoResponse();

        response.setId(entity.getId());
        response.setSubCategoryName(entity.getSubCategoryName());

        return response;
    }

    @Override
    public SubCategoryDtoRequest toDtoRequest(SubCategory entity) {
        SubCategoryDtoRequest request = new SubCategoryDtoRequest();

        request.setId(entity.getId());
        request.setSubCategoryName(entity.getSubCategoryName());
        request.setProducts(entity.getProducts());

        return request;
    }

    @Override
    public SubCategory responseToEntity(SubCategoryDtoResponse resp) {
        SubCategory subCategory = new SubCategory();

        subCategory.setId(resp.getId());
        subCategory.setSubCategoryName(resp.getSubCategoryName());

        return subCategory;
    }

    @Override
    public SubCategory requestToEntity(SubCategoryDtoRequest req) {
        SubCategory subCategory = new SubCategory();

        subCategory.setSubCategoryName(req.getSubCategoryName());
        subCategory.setProducts(req.getProducts());

        return subCategory;
    }
}
