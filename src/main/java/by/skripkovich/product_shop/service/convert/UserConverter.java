package by.skripkovich.product_shop.service.convert;

import by.skripkovich.product_shop.dao.dto.UserDtoRequest;
import by.skripkovich.product_shop.dao.dto.UserDtoResponse;
import by.skripkovich.product_shop.dao.entity.User;
import org.springframework.stereotype.Service;

@Service
public class UserConverter implements CommonConvert<User, UserDtoResponse, UserDtoRequest> {

    @Override
    public UserDtoResponse toDtoResponse(User entity) {
        UserDtoResponse response = new UserDtoResponse();

        response.setId(entity.getId());
        response.setName(entity.getName());
        response.setEmail(entity.getEmail());
        response.setAddress(entity.getAddress());
        response.setLevelAccess(entity.getLevelAccess());
        response.setOrders(entity.getOrders());
        response.setNumberPhone(entity.getNumberPhone());

        return response;
    }

    @Override
    public UserDtoRequest toDtoRequest(User entity) {
        UserDtoRequest request = new UserDtoRequest();

        request.setEmail(entity.getEmail());
        request.setId(entity.getId());
        request.setLevelAccess(entity.getLevelAccess());
        request.setName(entity.getName());
        request.setOrders(entity.getOrders());
        request.setPassword(entity.getPassword());
        request.setNumberPhone(entity.getNumberPhone());
        request.setAddress(entity.getAddress());

        return request;
    }

    @Override
    public User responseToEntity(UserDtoResponse resp) {
        User user = new User();

        user.setId(resp.getId());
        user.setAddress(resp.getAddress());
        user.setEmail(resp.getEmail());
        user.setLevelAccess(resp.getLevelAccess());
        user.setName(resp.getName());
        user.setOrders(resp.getOrders());
        user.setNumberPhone(resp.getNumberPhone());

        return user;
    }

    @Override
    public User requestToEntity(UserDtoRequest req) {
        User user = new User();

        user.setPassword(req.getPassword());
        user.setOrders(req.getOrders());
        user.setName(req.getName());
        user.setLevelAccess(req.getLevelAccess());
        user.setEmail(req.getEmail());
        user.setAddress(req.getAddress());
        user.setNumberPhone(req.getNumberPhone());
        user.setId(req.getId());

        return user;
    }
}
