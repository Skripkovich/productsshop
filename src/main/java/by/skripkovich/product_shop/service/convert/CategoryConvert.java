package by.skripkovich.product_shop.service.convert;

import by.skripkovich.product_shop.dao.dto.CategoryDtoRequest;
import by.skripkovich.product_shop.dao.dto.CategoryDtoResponse;
import by.skripkovich.product_shop.dao.dto.SubCategoryDtoResponse;
import by.skripkovich.product_shop.dao.entity.Category;
import by.skripkovich.product_shop.dao.entity.SubCategory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryConvert implements CommonConvert<Category, CategoryDtoResponse, CategoryDtoRequest> {

    @Override
    public CategoryDtoResponse toDtoResponse(Category entity) {
        CategoryDtoResponse response = new CategoryDtoResponse();
        List<SubCategoryDtoResponse> subCategoryDtoResponseList = new ArrayList<>();

        entity.getSubCategories().forEach(subCategory ->
                subCategoryDtoResponseList.add(new SubCategoryDtoResponse(subCategory.getId(), subCategory.getSubCategoryName())));

        response.setId(entity.getId());
        response.setCategoryName(entity.getCategoryName());
        response.setSubCategories(subCategoryDtoResponseList);

        return response;
    }

    @Override
    public CategoryDtoRequest toDtoRequest(Category entity) {
        CategoryDtoRequest request = new CategoryDtoRequest();

        request.setId(entity.getId());
        request.setCategoryName(entity.getCategoryName());
        request.setSubCategories(entity.getSubCategories());

        return request;
    }

    @Override
    public Category responseToEntity(CategoryDtoResponse resp) {
        Category category = new Category();
        List<SubCategory> subCategoryList = new ArrayList<>();

        resp.getSubCategories().forEach(subCategory ->
                subCategoryList.add(new SubCategory(subCategory.getId(), subCategory.getSubCategoryName())));

        category.setId(resp.getId());
        category.setCategoryName(resp.getCategoryName());
        category.setSubCategories(subCategoryList);

        return category;
    }

    @Override
    public Category requestToEntity(CategoryDtoRequest req) {
        Category category = new Category();

        category.setId(req.getId());
        category.setCategoryName(req.getCategoryName());
        category.setSubCategories(req.getSubCategories());

        return category;
    }
}
