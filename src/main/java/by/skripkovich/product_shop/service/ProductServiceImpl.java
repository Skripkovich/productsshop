package by.skripkovich.product_shop.service;

import by.skripkovich.product_shop.dao.dto.ProductDtoRequest;
import by.skripkovich.product_shop.dao.dto.ProductDtoResponse;
import by.skripkovich.product_shop.dao.entity.Product;
import by.skripkovich.product_shop.dao.entity.SubCategory;
import by.skripkovich.product_shop.dao.repository.ProductRepository;
import by.skripkovich.product_shop.dao.repository.SubCategoryRepository;
import by.skripkovich.product_shop.exception.ResourceNotFoundException;
import by.skripkovich.product_shop.service.convert.CommonConvert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository repository;
    @Autowired
    private CommonConvert<Product, ProductDtoResponse, ProductDtoRequest> commonConvert;
    @Autowired
    private SubCategoryRepository subCategoryRepository;


    @Override
    public ProductDtoResponse findById(Long id) {
        Product product = repository.findById(id).orElseThrow(() -> new ResourceNotFoundException(id));

        return commonConvert.toDtoResponse(product);
    }

    @Override
    public List<ProductDtoResponse> findAll() {
        List<ProductDtoResponse> responses = new ArrayList<>();

        repository.findAll().forEach((p) -> commonConvert.toDtoResponse(p));

        return responses;
    }

    @Override
    public ProductDtoResponse save(ProductDtoRequest request) {
        return commonConvert.toDtoResponse(repository.save(commonConvert.requestToEntity(request)));
    }

    @Override
    public void update(ProductDtoRequest request) {
        repository.findById(request.getId()).orElseThrow(() -> new ResourceNotFoundException(request));
        repository.save(commonConvert.requestToEntity(request));
    }

    @Override
    public void delete(Long id) {
        repository.deleteById(id);
    }

    @Override
    public List<ProductDtoResponse> findBySubCategory(String subCategoryName) {
        List<ProductDtoResponse> responses = new ArrayList<>();
        List<SubCategory> subCategoryList = subCategoryRepository.findBySubCategoryName(subCategoryName);
        subCategoryList.forEach(subCategory -> repository.findBySubCategoryId(subCategory.getId()).
                forEach(product -> responses.add(commonConvert.toDtoResponse(product))));


        return responses;
    }
}
