package by.skripkovich.product_shop.service;

import by.skripkovich.product_shop.dao.dto.CategoryDtoRequest;
import by.skripkovich.product_shop.dao.dto.CategoryDtoResponse;

import java.util.List;
import java.util.Set;

public interface CategoryService {
    CategoryDtoResponse findById (Long id);

    List<CategoryDtoResponse> findAll();

    CategoryDtoResponse save(CategoryDtoRequest category);

    void update(CategoryDtoRequest category);

    void delete(Long id);

    void saveAll (List<CategoryDtoRequest> category);

    Set<CategoryDtoResponse> getCategory ();
}
