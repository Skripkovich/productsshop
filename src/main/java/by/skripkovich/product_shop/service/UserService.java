package by.skripkovich.product_shop.service;

import by.skripkovich.product_shop.dao.dto.OrderDtoRequest;
import by.skripkovich.product_shop.dao.dto.UserDtoRequest;
import by.skripkovich.product_shop.dao.dto.UserDtoResponse;

import java.util.List;

public interface UserService {

    UserDtoResponse findById(Long id);

    List<UserDtoResponse> findAll();

    UserDtoResponse save(UserDtoRequest request);

    void update(UserDtoRequest request);

    void delete(Long id);

    void createNewUser(UserDtoRequest request);

    UserDtoResponse authorization(UserDtoRequest request);

    UserDtoRequest findByNumberPhone(String numberPhone);

    void addOrder(OrderDtoRequest request);
}
