package by.skripkovich.product_shop.service;

import by.skripkovich.product_shop.dao.dto.OrderDtoRequest;
import by.skripkovich.product_shop.dao.dto.OrderDtoResponse;
import by.skripkovich.product_shop.dao.entity.Order;
import by.skripkovich.product_shop.dao.repository.OrderRepository;
import by.skripkovich.product_shop.exception.ResourceNotFoundException;
import by.skripkovich.product_shop.service.convert.CommonConvert;
import by.skripkovich.product_shop.type.OrderStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private OrderRepository orderRepository;
    private CommonConvert<Order, OrderDtoResponse, OrderDtoRequest> commonConvert;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, CommonConvert<Order, OrderDtoResponse, OrderDtoRequest> commonConvert) {
        this.orderRepository = orderRepository;
        this.commonConvert = commonConvert;
    }

    @Override
    public OrderDtoResponse findById(Long numberOrder) {
        Order order = orderRepository.findById(numberOrder).orElseThrow(() -> new ResourceNotFoundException(numberOrder));
        return commonConvert.toDtoResponse(order);
    }

    @Override
    public List<OrderDtoResponse> findAll() {
        List<OrderDtoResponse> responses = new ArrayList<>();
        orderRepository.findAll().forEach((e) -> responses.add(commonConvert.toDtoResponse(e)));
        return responses;
    }

    @Override
    public OrderDtoResponse save(OrderDtoRequest order) {
        order.setDate(new Date());
        order.setOrderStatus(OrderStatus.PROCESSED);
        Order newOrder = orderRepository.save(commonConvert.requestToEntity(order));
        return commonConvert.toDtoResponse(newOrder);
    }

    @Override
    public void update(OrderDtoRequest order) {
        orderRepository.findById(order.getNumberOrder()).orElseThrow(() -> new ResourceNotFoundException(order));
        orderRepository.save(commonConvert.requestToEntity(order));

    }

    @Override
    public void delete(Long numberOrder) {
        orderRepository.deleteById(numberOrder);
    }

    @Override
    public void saveAll(List<OrderDtoRequest> order) {
        List<Order> list = new ArrayList<>();
        order.forEach(orderDtoRequest -> list.add(commonConvert.requestToEntity(orderDtoRequest)));
        orderRepository.saveAll(list);
    }
}
