package by.skripkovich.product_shop.service;

import by.skripkovich.product_shop.dao.dto.CategoryDtoRequest;
import by.skripkovich.product_shop.dao.dto.CategoryDtoResponse;
import by.skripkovich.product_shop.dao.dto.SubCategoryDtoRequest;
import by.skripkovich.product_shop.dao.dto.SubCategoryDtoResponse;
import by.skripkovich.product_shop.dao.entity.Category;
import by.skripkovich.product_shop.dao.entity.SubCategory;
import by.skripkovich.product_shop.dao.repository.CategoryRepository;
import by.skripkovich.product_shop.dao.repository.SubCategoryRepository;
import by.skripkovich.product_shop.exception.ResourceNotFoundException;
import by.skripkovich.product_shop.service.convert.CommonConvert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SubCategoryServiceImpl implements SubCategoryService{

    @Autowired
    private SubCategoryRepository subCategoryRepository;
    @Autowired
    private CommonConvert<SubCategory, SubCategoryDtoResponse, SubCategoryDtoRequest> commonConvert;

    @Override
    public SubCategoryDtoResponse findById(Long id) {
        SubCategory subCategory = subCategoryRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(id));
        return commonConvert.toDtoResponse(subCategory);
    }

    @Override
    public List<SubCategoryDtoResponse> findAll() {
        List<SubCategoryDtoResponse> responses = new ArrayList<>();
        subCategoryRepository.findAll().forEach((e) -> responses.add(commonConvert.toDtoResponse(e)));
        return responses;
    }

    @Override
    public SubCategoryDtoResponse save(SubCategoryDtoRequest subCategory) {
        SubCategory newSubCategory = subCategoryRepository.save(commonConvert.requestToEntity(subCategory));
        return commonConvert.toDtoResponse(newSubCategory);
    }

    @Override
    public void update(SubCategoryDtoRequest subCategory) {
        subCategoryRepository.findById(subCategory.getId()).orElseThrow(() -> new ResourceNotFoundException(subCategory));
        subCategoryRepository.save(commonConvert.requestToEntity(subCategory));
    }

    @Override
    public void delete(Long id) {
        subCategoryRepository.deleteById(id);
    }

    @Override
    public void saveAll(List<SubCategoryDtoRequest> subCategory) {
        List<SubCategory> list = new ArrayList<>();
        subCategory.forEach(subCategoryDtoRequest -> list.add(commonConvert.requestToEntity(subCategoryDtoRequest)));
        subCategoryRepository.saveAll(list);
    }
}
