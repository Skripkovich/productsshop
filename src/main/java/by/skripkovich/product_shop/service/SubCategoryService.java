package by.skripkovich.product_shop.service;

import by.skripkovich.product_shop.dao.dto.CategoryDtoRequest;
import by.skripkovich.product_shop.dao.dto.CategoryDtoResponse;
import by.skripkovich.product_shop.dao.dto.SubCategoryDtoRequest;
import by.skripkovich.product_shop.dao.dto.SubCategoryDtoResponse;

import java.util.List;

public interface SubCategoryService {

    SubCategoryDtoResponse findById (Long id);

    List<SubCategoryDtoResponse> findAll();

    SubCategoryDtoResponse save(SubCategoryDtoRequest subCategory);

    void update(SubCategoryDtoRequest subCategory);

    void delete(Long id);

    void saveAll (List<SubCategoryDtoRequest> subCategory);
}
