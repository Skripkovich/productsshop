package by.skripkovich.product_shop.service;

import by.skripkovich.product_shop.dao.dto.CategoryDtoRequest;
import by.skripkovich.product_shop.dao.dto.CategoryDtoResponse;
import by.skripkovich.product_shop.dao.entity.Category;
import by.skripkovich.product_shop.dao.repository.CategoryRepository;
import by.skripkovich.product_shop.exception.ResourceNotFoundException;
import by.skripkovich.product_shop.service.convert.CommonConvert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private CommonConvert<Category, CategoryDtoResponse, CategoryDtoRequest> commonConvert;

    @Override
    public CategoryDtoResponse findById(Long id) {
        Category category = categoryRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(id));
        return commonConvert.toDtoResponse(category);
    }

    @Override
    public List<CategoryDtoResponse> findAll() {
        List<CategoryDtoResponse> responses = new ArrayList<>();
        categoryRepository.findAll().forEach((e) -> responses.add(commonConvert.toDtoResponse(e)));
        return responses;
    }

    @Override
    public CategoryDtoResponse save(CategoryDtoRequest category) {
        Category newCategory = categoryRepository.save(commonConvert.requestToEntity(category));
        return commonConvert.toDtoResponse(newCategory);
    }

    @Override
    public void update(CategoryDtoRequest category) {
        categoryRepository.findById(category.getId()).orElseThrow(() -> new ResourceNotFoundException(category));
        categoryRepository.save(commonConvert.requestToEntity(category));
    }

    @Override
    public void delete(Long id) {
        categoryRepository.deleteById(id);
    }

    @Override
    public void saveAll(List<CategoryDtoRequest> category) {
        List<Category> list = new ArrayList<>();
        category.forEach(categoryDtoRequest -> list.add(commonConvert.requestToEntity(categoryDtoRequest)));
        categoryRepository.saveAll(list);
    }

    @Override
    public Set<CategoryDtoResponse> getCategory() {
        Set<CategoryDtoResponse> response = new HashSet<>();
        categoryRepository.findAll().forEach(category -> response.add(commonConvert.toDtoResponse(category)));

        return response;
    }
}
