package by.skripkovich.product_shop.service.datageneration;

import by.skripkovich.product_shop.dao.entity.Category;
import by.skripkovich.product_shop.dao.entity.Product;
import by.skripkovich.product_shop.dao.entity.SubCategory;
import by.skripkovich.product_shop.dao.repository.CategoryRepository;
import by.skripkovich.product_shop.dao.repository.ProductRepository;
import by.skripkovich.product_shop.dao.repository.SubCategoryRepository;
import by.skripkovich.product_shop.type.UnitType;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Component
public class DataBaseGeneration {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private SubCategoryRepository subCategoryRepository;


    @EventListener({ContextRefreshedEvent.class})
    public void generate() {
        categoryGeneration();
    }

    private void categoryGeneration() {
        Faker faker = new Faker();
        List<Category> categoryList = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            Category category = new Category();
            category.setCategoryName(faker.pokemon().name());
            category.setSubCategories(subCategoryGeneration());

            categoryList.add(category);
        }
        categoryRepository.saveAll(categoryList);
    }

    private List<SubCategory> subCategoryGeneration() {
        int random = ThreadLocalRandom.current().nextInt(1, 5);
        Faker faker = new Faker();
        List<SubCategory> subCategoryList = new ArrayList<>();

        for (int i = 0; i < random; i++) {
            SubCategory subCategory = new SubCategory();
            subCategory.setSubCategoryName(faker.animal().name());
            subCategory.setProducts(productGenerate());

            subCategoryList.add(subCategory);
        }

        return subCategoryList;
    }

    private List<Product> productGenerate() {
        int random = ThreadLocalRandom.current().nextInt(1, 5);
        Faker faker = new Faker();
        List<Product> list = new ArrayList<>();

        for (int i = 0; i < random; i++) {
            Product product = new Product();
            product.setWeight((float) ThreadLocalRandom.current().nextDouble(0, 5));
            product.setUnitType(UnitType.PIECES);
            product.setProductName(faker.food().fruit());
            product.setPrice(BigDecimal.valueOf(ThreadLocalRandom.current().nextDouble(0, 500)));
            product.setDescription(faker.food().vegetable());
            product.setImage(faker.file().fileName());

            list.add(product);
        }
        productRepository.saveAll(list);
        return list;
    }
}
