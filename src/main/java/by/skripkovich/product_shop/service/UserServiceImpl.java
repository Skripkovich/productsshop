package by.skripkovich.product_shop.service;

import by.skripkovich.product_shop.dao.dto.OrderDtoRequest;
import by.skripkovich.product_shop.dao.dto.OrderDtoResponse;
import by.skripkovich.product_shop.dao.dto.UserDtoRequest;
import by.skripkovich.product_shop.dao.dto.UserDtoResponse;
import by.skripkovich.product_shop.dao.entity.Order;
import by.skripkovich.product_shop.dao.entity.User;
import by.skripkovich.product_shop.dao.repository.UserRepository;
import by.skripkovich.product_shop.exception.ResourceNotFoundException;
import by.skripkovich.product_shop.exception.WrongNumberPhone;
import by.skripkovich.product_shop.service.convert.CommonConvert;
import by.skripkovich.product_shop.type.OrderStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final CommonConvert<User, UserDtoResponse, UserDtoRequest> userConvert;
    private final CommonConvert<Order, OrderDtoResponse, OrderDtoRequest> orderConvert;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, CommonConvert<User, UserDtoResponse, UserDtoRequest> userConvert,
                           CommonConvert<Order, OrderDtoResponse, OrderDtoRequest> orderConvert) {
        this.userRepository = userRepository;
        this.userConvert = userConvert;
        this.orderConvert = orderConvert;
    }

    @Override
    public UserDtoResponse findById(Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(id));

        return userConvert.toDtoResponse(user);
    }

    @Override
    public List<UserDtoResponse> findAll() {
        List<UserDtoResponse> list = new ArrayList<>();
        userRepository.findAll().forEach((user -> list.add(userConvert.toDtoResponse(user))));
        return list;
    }

    @Override
    public UserDtoResponse save(UserDtoRequest request) {
        User user = userRepository.save(userConvert.requestToEntity(request));

        return userConvert.toDtoResponse(user);
    }

    @Override
    public void update(UserDtoRequest request) {
        userRepository.findById(request.getId()).orElseThrow(() -> new ResourceNotFoundException(request.getId()));
        userRepository.save(userConvert.requestToEntity(request));
    }

    @Override
    public void delete(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public void createNewUser(UserDtoRequest request) {
        if (userRepository.findByEmail(request.getEmail()).isPresent()) {
            //TODO создать свой эксепшн
            throw new RuntimeException("This email is used");
        } else
            save(request);
    }

    //TODO создать свой эксепшн
    @Override
    public UserDtoResponse authorization(UserDtoRequest request) {

        User user = userRepository.findByEmail(request.getEmail())
                .orElseThrow(() -> new RuntimeException("Wrong email or password"));

        if (user.getPassword().equals(request.getPassword())) {
            return userConvert.toDtoResponse(user);
        } else
            throw new RuntimeException("Wrong email or password");
    }

    @Override
    public UserDtoRequest findByNumberPhone(String numberPhone) {

        User user = userRepository.findByNumberPhone(numberPhone)
                .orElseThrow(() -> new WrongNumberPhone(numberPhone));

        return userConvert.toDtoRequest(user);
    }

    @Override
    public void addOrder(OrderDtoRequest request) {
        UserDtoRequest user = findByNumberPhone(request.getNumberPhone());
        request.setDate(new Date());
        request.setOrderStatus(OrderStatus.PROCESSED);
        user.getOrders().add(orderConvert.requestToEntity(request));
        update(user);
    }
}
