package by.skripkovich.product_shop.service;

import by.skripkovich.product_shop.dao.dto.ProductDtoRequest;
import by.skripkovich.product_shop.dao.dto.ProductDtoResponse;

import java.util.List;

public interface ProductService {

    ProductDtoResponse findById(Long id);

    List<ProductDtoResponse> findAll();

    ProductDtoResponse save(ProductDtoRequest request);

    void update(ProductDtoRequest request);

    void delete(Long id);

    List<ProductDtoResponse>findBySubCategory(String subCategory);
}
