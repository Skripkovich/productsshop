package by.skripkovich.product_shop.service;

import by.skripkovich.product_shop.dao.dto.OrderDtoRequest;
import by.skripkovich.product_shop.dao.dto.OrderDtoResponse;

import java.util.List;

public interface OrderService {
    OrderDtoResponse findById(Long id);

    List<OrderDtoResponse> findAll();

    OrderDtoResponse save(OrderDtoRequest order);

    void update(OrderDtoRequest order);

    void delete(Long id);

    void saveAll (List<OrderDtoRequest> order);
}
